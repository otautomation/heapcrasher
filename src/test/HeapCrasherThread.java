package test;

import java.util.Random;
import java.util.Map;
import java.util.HashMap;

public class HeapCrasherThread extends Thread 
{
	    public static Map sink = new HashMap();
			
		int memsize = 100000;
		int cleanperc = 100;
		int cleantime = 100;
		
		String threadName = "x";
		
		long lastheapsize = 0;
		
		Runtime r = Runtime.getRuntime();
		
	    public HeapCrasherThread(String str, int msize, int cperc, int ctime) {
		   super(str);
		   memsize = msize;
		   threadName = str;
		   cleanperc = cperc;
		   cleantime = ctime;
	    }
	    
		public static void useMem(String xxx)
		{
			StringBuffer x = new StringBuffer();	
			x.append(xxx);	
			String y = x.toString();
		}
		
	    public void run() 
	    {
			long heapMaxSize = Runtime.getRuntime().maxMemory();
						
			int count = 1;
			
			Map threadsink = (Map) HeapCrasherThread.sink.get(threadName);
			if (threadsink == null)
			{
				threadsink = new HashMap();
				HeapCrasherThread.sink.put(threadName,threadsink);
			}
			
			Random randomGenerator = new Random();
			
			int sinksize = randomGenerator.nextInt(250);
    	
			while (true)
			{								
				StringBuffer x = new StringBuffer();
				for (int i=0; i <= memsize; i++)
				{
					int randomInt = randomGenerator.nextInt(250);
					
					//x.append('X');
					x.append( (char) randomInt);
				}
				
				
				//
				//  Wipe the old hashmap 5/100 iterations  (should be abot every 20 seconds)
				//
				int cleartest = randomGenerator.nextInt(100);
				
				if ( cleartest < cleanperc)
				{
					System.out.println ( threadName + " WIPING SINK");
					threadsink = new HashMap();
					HeapCrasherThread.sink.put(threadName,threadsink);
				}

				if ( count > cleantime)
				{
					count = 0;
					System.out.println ( threadName + " WIPING SINK");
					threadsink = new HashMap();
					HeapCrasherThread.sink.put(threadName,threadsink);
				}

				
				//useMem(x.toString());

				Integer sinkidx = new Integer(randomGenerator.nextInt(sinksize));
				
				String putStr = (String) threadsink.get(sinkidx);
				if (putStr == null)
				{
					threadsink.put(sinkidx,x.toString());
				}							
				
				long heapsize = r.totalMemory();
				long heapFreeSize = r.freeMemory();

				String expandcontract = "";
				if (heapsize > lastheapsize)
				{
					expandcontract = "      **** EXPAND ******";
				}
				else
				{
					if (heapsize < lastheapsize)
					{
						expandcontract = "      >>>>>>>>>>  CONTRACT >>>>>>>>";
					}	 
				}
				System.out.println ( threadName + " -Iteration " + count + " - " + threadsink.size()  + " - " + heapFreeSize + " < " + heapsize + " < " + heapMaxSize + " - " +expandcontract);
				
				
				lastheapsize = heapsize;
				
				count++;	
				
	              try {
				        sleep((int)(Math.random() * 1000));
			          }      
		              catch (InterruptedException e) {
		              }
			}
			
	    }
	
}
