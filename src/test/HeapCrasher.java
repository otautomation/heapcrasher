package test;

import java.util.Random;

public class HeapCrasher
{
	
	
	public static void main(String[] args)
	{		

		System.out.println ( "Number of args = " + args.length);
		int threads = Integer.parseInt(args[0]);
		int memsize = Integer.parseInt(args[1]);
		int cleanperc = Integer.parseInt(args[2]);
		int cleantime = Integer.parseInt(args[3]);

		Random randomGenerator = new Random();

			
        for (int i = 0; i<= threads; i++)
        {
           String tname = "T" + i;
           
   		   int randomInt = randomGenerator.nextInt(memsize);      	 
		   new HeapCrasherThread(tname,randomInt, cleanperc, cleantime).start();
        }
       
	}
	
	
	
	
	
	
	
	
	
	
	
}
